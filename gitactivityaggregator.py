#!/usr/bin/python3

from githubParser import getActivity as githubActivity
from gitlabParser import getActivity as gitlabActivity
from datetime import datetime
import sys

def mergeDict(dict1, dict2):
    dict3 = {**dict1, **dict2}
    for key, value in dict3.items():
        if key in dict1 and key in dict2:
            dict3[key] = value + dict1[key]

    return dict3

def getAggregatedActivity(username):
    totalActivity = mergeDict(githubActivity(username), gitlabActivity(username))
    return totalActivity

def toDate(date):
    return datetime.strptime(date, '%Y-%m-%d')

def activitesToDateDicitonnary(dict):
    dict2 = {}
    for key, value in dict.items():
        dict2[toDate(key)] = value
    return dict2

def getOldestActivity(dict):
    older = None;
    for key in dict.keys():
        if(older == None or key < older):
            older = key
    return older


englishWeekDays = ["M","T","W","T","F","S","S"]

def displayActivities(username):
    print("Displaying activity for: " + username)
    activites = activitesToDateDicitonnary(getAggregatedActivity(username))
    datesByWeekday = {}
    for dateAct in activites.keys():
        if( dateAct.weekday() not in datesByWeekday.keys()):
            datesByWeekday[dateAct.weekday()] = [dateAct]
        else:
            datesByWeekday[dateAct.weekday()] += [dateAct]

    for dayOfWeekAct in  sorted(datesByWeekday):
        print (englishWeekDays[dayOfWeekAct], end='   ')
        if (dayOfWeekAct != 6):
            print ("  ",end='|')
        for activityDay in sorted(datesByWeekday[dayOfWeekAct]):
            if (activites[activityDay] > 9):
                print (activites[activityDay],end='|')
            else:
                print (str(activites[activityDay]) + " ",end='|')
        print()
        
displayActivities(sys.argv[1])
