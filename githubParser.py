#!/usr/bin/python3

from urllib.request import urlopen
from bs4 import BeautifulSoup

def getActivity(username):
    request_page = urlopen("https://github.com/" + username)
    html_result = request_page.read()
    request_page.close()
    html_soup = BeautifulSoup(html_result, "html.parser")

    annual_activity = {}

    for calendar in html_soup.find_all("svg", class_="js-calendar-graph-svg"):
        for day_activity in calendar.find_all("rect"):
            annual_activity[str(day_activity['data-date'])] = int(day_activity['data-count'])
    return annual_activity
