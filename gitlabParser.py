import json

from urllib.request import urlopen

def getActivity(username):
    request_page = urlopen("https://gitlab.com/users/"+ username +"/calendar.json")
    html_result = request_page.read()
    request_page.close()
    activity = json.loads(html_result)
    activityDict = {}
    for k, v in zip(activity.keys(), activity.values()):
        activityDict[str(k)] = int(v)
    return activityDict
