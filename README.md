A simple tool to aggregate your activity from gitlab and github in an unified calendar.

## Screenshot
![a screenshot of the app](example.png "A screenshot of the app")

### Installation
```
sudo apt install python3
sudo apt install pip
pip install beautifulsoup4

git clone git@gitlab.com:devauxcedric/git-activity-aggregator.git
cd git-activity-aggregator
```

### Usage
```
python3 gitactivityaggregator.py "username"
```

### Removal
```
rm -rf git-activity-aggregator
sudo apt remove python3
sudo apt remove pip

sudo apt autoremove
```
